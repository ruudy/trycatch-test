<?php

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app->get('/', function() use ($app) {
    return $app->escape('Hello TryCatch');
});

// Routes from different packets
require_once __DIR__ . '/../src/Task_Two/config/routes.php';
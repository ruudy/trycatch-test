<?php

$app->register(new Silex\Provider\ServiceControllerServiceProvider());

// Services from different packets
require_once __DIR__ . '/../src/Task_Two/config/services.php';
TryCatch Exercise
=================

Instalation
-----------

Install composer (https://getcomposer.org/download/)
    
    $ curl -sS https://getcomposer.org/installer | php
    $ ./composer.phar install
    
Create the database and configure the file:

    config/database.php
    
Run the database install and seeder for test purposes

    php index.php trycatch:database:create
    php index.php trycatch:database:seed
    

Explanation
-----------

This project includes silex microframework, only needed on Task Two exercises.

This silex version dont include any skeleton of application, so all the project outside vendors folders is created from scratch.

I tried to include TDD (Test-driven development) in the project, but was hard for some actions (CRUD) in this basic project, because is unfinished class without views. 

Usage
-----

*Exercise A*

    php index.php a:run
    php index.php a:run --extra

*Exercise B*

 Using php pow function

    php index.php b:run

 Normal Script
     
    php index.php b:run --extra
    
 Recursive Script

    php index.php b:run --extra2
    
*Exercise C*

    php index.php c:run
    php index.php c:run --extra
    
*Task Two - Architectural struggle*

Php 5.4 is required on this project so you just need to:

    php -S localhost:8080 -t web web/index.php

And go to:

    http://localhost:8080
    
To see the Refactorized result go:

    http://localhost:8080/api/address/1
    
*Desert - The icing on the cake*

You can access the CRUD calls as restfull calls. For test non get calls i recommend to use chrome Postman extension.

List 

    METHODS (GET)
    http://localhost:8080/address
    
Show

    METHODS (GET)
    http://localhost:8080/address/1
    
Create

    METHODS (GET / POST)
    http://localhost:8080/address/create

Edit

    METHODS (GET / POST)
    http://localhost:8080/address/1/edit

Delete DELETE

    METHODS (DELETE)
    http://localhost:8080/address/1

*Testing*

For run the unit test you can just run this command in the project root:

    vendor/bin/phpunit src/Task_Two/Tests/
    
TODO

1. Set --bootstrap option on phpunit to load autoload or bootstrap file and have all the classes loaded
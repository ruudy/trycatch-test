<?php
// index.php

require_once __DIR__.'/../vendor/autoload.php';

//use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\BinaryFileResponse;

$app = new Silex\Application();

require __DIR__.'/../config/app.php';

$app->run();
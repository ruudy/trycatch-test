<?php

// Json Restful API
$app->get('/api/address/{id}', 'api.address.controller:getAddress')
    ->assert('id', '\d+');

// Restful
$app->get('/address', 'address.controller:getList')
    ->bind("address");

$app->get('/address/{id}', 'address.controller:getShow')
    ->assert('id', '\d+');

$app->get('/address/create', 'address.controller:getCreate');
$app->post('/address/create', 'address.controller:postCreate');

$app->get('/address/{id}/edit', 'address.controller:getEdit');
$app->post('/address/{id}/edit', 'address.controller:postEdit');

$app->delete('/address/{id}', 'address.controller:delete');

<?php

use TryCatch\Task_Two\Services\CsvParser;
use TryCatch\Task_Two\Controllers\Api\v1\AddressController as apiAddressController;
use TryCatch\Task_Two\Controllers\AddressController;
use Symfony\Component\HttpFoundation\Request;

$app['csv.parser'] = $app->share(function() {
    return new CsvParser;
});

// Api
$app['api.address.controller'] = $app->share(function() use ($app) {
    return new apiAddressController($app['csv.parser']);
});

// Cruds
$app['address.controller'] = $app->share(function() use ($app) {
    return new AddressController($app);
});

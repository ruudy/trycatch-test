<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 28/09/14
 * Time: 12:00
 */

namespace TryCatch\Task_Two\Services;

use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\Response;

class CsvParser
{
    /**
     * Parse an address CSV file, you can use this function passing the entire path to an csv you upload by any method
     * (dont modify the file inside examples, testing purposes)
     *
     * @param $filePath
     * @return array
     */
    public function parse($filePath)
    {
        $result = [];

        try {
            $file = fopen($filePath, 'r');
        }
        catch (FileNotFoundException $e) {
            new Response('Csv File not found');
        }

        while (($line = fgetcsv($file)) !== FALSE) {
            $result[] = $line;
        }

        fclose($file);

        return $result;
    }

    /**
     * If your CSV file is in utf8 and coma separated you can use this instead of normal function, this function is
     * optimized and will take less time, "parse" will have problems with CSV of thousands lines
     *
     * @param $filePath
     * @return array
     */
    public function advancedParse($filePath)
    {
        $result = array_map('str_getcsv', file($filePath));

        return $result;
    }

} 
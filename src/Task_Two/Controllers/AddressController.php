<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 28/09/14
 * Time: 13:43
 */

namespace TryCatch\Task_Two\Controllers;

use Illuminate\Database\QueryException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use TryCatch\Task_Two\Controllers\Interfaces\CrudInterface;

use TryCatch\Task_Two\Models\Address;

class AddressController extends BaseController implements CrudInterface
{
    /**
     * List all addresses stored in database
     *
     * @return JsonResponse
     */
    public function getList()
    {
        $addresses = Address::all()->toArray();

        // Result as json, TODO views
        return new JsonResponse(array('addresses' => $addresses));
    }

    /**
     * Show data from one address stored in database
     *
     * @param $id
     * @return JsonResponse|Response
     */
    public function getShow($id)
    {
        $address = Address::find($id);
        if (!$address) return new Response('Address not found');

        // Result as json, TODO views
        return new JsonResponse(array('address' => $address->toArray()));
    }

    /**
     * Display the create address form
     *
     * @return Response
     */
    public function getCreate()
    {
        return new Response('Display Create form');
    }

    /**
     * Create an address entity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function postCreate()
    {
        // TODO validate required request params

        var_dump($this->app['request']->get('name'));

        $address = new Address();
        try {
            $address->name = $this->app['request']->get('name');
            $address->phone = $this->app['request']->get('phone');
            $address->street = $this->app['request']->get('street');
            $address->save();
        }
        catch (QueryException $e) {
            return new Response('Some required field are missing');
        }

        return $this->app->redirect($this->app["url_generator"]->generate("address"));
    }

    /**
     * Display the edit address form
     *
     * @param $id
     * @return JsonResponse|Response
     */
    public function getEdit($id)
    {
        $address = Address::find($id);
        if (!$address) return new Response('Address not found');

        // Result as json, TODO views
        echo 'Display Create form with: ';

        return new JsonResponse(array('address' => $address->toArray()));
    }

    /**
     * Edit the data of an address entity
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function postEdit($id)
    {
        // TODO validate required request params

        $address = Address::find($id);
        if (!$address) return new Response('Address not found');

        try {
            $address->name = $this->app['request']->get('name');
            $address->phone = $this->app['request']->get('phone');
            $address->street = $this->app['request']->get('street');
            $address->save();
        }
        catch (QueryException $e) {
            return new Response('Some required field are missing');
        }

        return $this->app->redirect($this->app["url_generator"]->generate("address"));
    }

    /**
     * Delete an address of database
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Responseç
     */
    public function delete($id)
    {
        $address = Address::find($id);
        if (!$address) return new Response('Address not found');

        $address->delete($id);

        return new Response('Successfully deleted');
    }

} 
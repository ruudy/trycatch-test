<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 28/09/14
 * Time: 13:43
 */

namespace TryCatch\Task_Two\Controllers\Api\v1;

use Symfony\Component\HttpFoundation\Response;
use TryCatch\Task_Two\Controllers\Api\ApiController;
use TryCatch\Task_Two\Presenters\Csv\AddressPresenter;

class AddressController extends ApiController
{
    /**
     * Get the info of one address
     *
     * @param $id
     * @return JsonResponse|Response
     */
    public function getAddress($id)
    {
        $addresses = $this->csvParser->advancedParse(__DIR__ . '/../../../docs/addresses.csv');

        // I assume the id is auto increment and thats why is not included on the .csv, but is not a good practice
        if (empty($addresses[$id])) {
            return $this->error(
                1,
                'Address not found'
            );
        }

        $addressPresenter = new AddressPresenter($addresses[$id]);

        $addressPresenter->presentSingle();

        return $this->done(
            'Address info',
            $addressPresenter->getResult()
        );
    }

} 
<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 29/09/14
 * Time: 0:00
 */

namespace TryCatch\Task_Two\Controllers\Api;

use TryCatch\Task_Two\Services\CsvParser;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class ApiController
{
    protected $csvParser;

    public function __construct(CsvParser $csvParser)
    {
        $this->csvParser = $csvParser;
    }

    protected function done($message, $data = null)
    {
        return new JsonResponse(
            array(
                "code" => 0,
                "message" => $message,
                "data" => $data,
            )
        );
    }

    protected function error($code, $message, $data = null)
    {
        return new JsonResponse(
            array(
                "code" => $code,
                "message" => $message,
                "data" => $data,
            )
        );
    }
}
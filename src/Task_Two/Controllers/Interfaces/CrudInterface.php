<?php

namespace TryCatch\Task_Two\Controllers\Interfaces;

interface CrudInterface
{
    public function getList();

    public function getShow($id);

    public function getCreate();

    public function postCreate();

    public function getEdit($id);

    public function postEdit($id);

    public function delete($id);
} 
<?php

namespace TryCatch\Task_Two\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;

abstract class BaseController
{
    protected $request;
    protected $app;

    public function __construct(Application $app)
    {
        $this->request  = new Request();
        $this->app      = $app;
    }
} 
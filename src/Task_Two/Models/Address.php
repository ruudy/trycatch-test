<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 29/09/14
 * Time: 18:58
 */

namespace TryCatch\Task_Two\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * Model of Address
 *
 * Class Address
 */
class Address extends Model
{
    protected $table = "addresses";
    protected $fillable = ['name', 'phone', 'street'];
} 
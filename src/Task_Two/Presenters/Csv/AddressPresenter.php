<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 29/09/14
 * Time: 19:09
 */

namespace TryCatch\Task_Two\Presenters\Csv;

use TryCatch\Task_Two\Presenters\BasePresenter;
use TryCatch\Task_Two\Presenters\Interfaces\PresenterInterface;

/**
 * Presents data from an CSV readed
 *
 * Class AddressPresenter
 * @package TryCatch\Task_Two\Presenters\Csv
 */
class AddressPresenter extends BasePresenter implements PresenterInterface
{
    public function presentSingle()
    {
        $this->result = [
            'name' => $this->data[0],
            'phone' => $this->data[1],
            'street' => $this->data[2],
        ];
    }

    public function presentList()
    {
        $this->result = $this->data;

        foreach ($this->result as &$resultRow) {
            $resultRow['name']     = $resultRow[0];
            $resultRow['phone']    = $resultRow[1];
            $resultRow['street']   = $resultRow[2];
            unset($resultRow[0]);
            unset($resultRow[1]);
            unset($resultRow[2]);
        }

        return $this->data;
    }

}
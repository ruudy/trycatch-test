<?php

namespace TryCatch\Task_Two\Presenters\Interfaces;

interface PresenterInterface
{
    public function presentSingle();

    public function presentList();
}
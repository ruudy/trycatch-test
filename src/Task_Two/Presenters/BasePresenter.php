<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 29/09/14
 * Time: 19:27
 */

namespace TryCatch\Task_Two\Presenters;

abstract class BasePresenter
{
    protected $data;
    protected $result = array();

    /**
     * Data can be only one object or a collection
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return $this->result;
    }
} 
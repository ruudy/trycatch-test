<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 27/09/14
 * Time: 17:04
 */

namespace TryCatch\Task_Two\Tests;

use PHPUnit_Framework_TestCase;
use TryCatch\Task_Two\Controllers\AddressController;
use TryCatch\Task_Two\Services\CsvParser;

class taskTwoRefactoringTest extends PHPUnit_Framework_TestCase
{
    /**
     * TODO The test wont assert until solve all the coding errors on src/Task_Two_old/example.php
     */
    function testRefactoring()
    {
        // Old
        ob_start();
        include '../src/Task_Two_old/example.php';
        // TODO set $_GET['id'] as 1
        $oldResult = ob_get_contents();
        ob_end_flush();

        // Refatored
        $csvParser = new CsvParser();
        $addressController = new AddressController($csvParser);
        $refactoredResult = $addressController->getAddress(1);

        $this->assertEquals($oldResult, $refactoredResult);
    }
} 
<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 28/09/14
 * Time: 23:25
 */

namespace TryCatch\Task_Two\Tests\Api;

use PHPUnit_Framework_TestCase;
use TryCatch\Task_Two\Controllers\Api\v1\AddressController;
use TryCatch\Task_Two\Services\CsvParser;
use Symfony\Component\HttpFoundation\JsonResponse;

class AddressControllerTest extends PHPUnit_Framework_TestCase
{
    protected $csvParser;

    protected function setUp()
    {
        $this->csvParser = new CsvParser();
    }

    public function testGetAddress()
    {
        $result = new JsonResponse([
            'code' => 0,
            'message' => "Address info",
            'data' => [
                'name' => "Michal",
                'phone' => "506088156",
                'street' =>"Michalowskiego 41",
            ]
        ]);

        $addressController = new AddressController($this->csvParser);

        // TODO cant assert JsonResponse, but not give failure, risky result
        $this->assertEquals(
            $result,
            $addressController->getAddress(0)
        );
    }
} 
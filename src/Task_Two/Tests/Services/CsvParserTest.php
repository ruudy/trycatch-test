<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 28/09/14
 * Time: 12:03
 */

namespace TryCatch\Task_Two\Tests\Services;

use PHPUnit_Framework_TestCase;
use TryCatch\Task_Two\Services\CsvParser;

class CsvParserTest extends PHPUnit_Framework_TestCase
{
    protected $result;

    protected function setUp()
    {
        $this->result = [
            [
                "Michal",
                "506088156",
                "Michalowskiego 41",
            ],
            [
                "Marcin",
                "502145785",
                "Opata Rybickiego 1",
            ],
            [
                "Piotr",
                "504212369",
                "Horacego 23",
            ],
            [
                "Albert",
                "605458547",
                "Jan Pawła 67",
            ],
        ];
    }

    /**
     * Test the parseAddressList of the CsvParser Service
     */
    public function testParse()
    {
        $csvParser = new CsvParser();

        $this->assertEquals(
            $this->result,
            $csvParser->parse(__DIR__ . '/../../docs/examples/example.csv')
        );
    }

    /**
     * Test the advancedParseAddressList of the CsvParser Service
     */
    public function testAdvancedParse()
    {
        $csvParser = new CsvParser();

        $this->assertEquals(
            $this->result,
            $csvParser->advancedParse(__DIR__ . '/../../docs/examples/example.csv')
        );
    }
} 
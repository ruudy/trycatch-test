<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 28/09/14
 * Time: 23:25
 */

namespace TryCatch\Task_Two\Tests\Controllers;

use Illuminate\Database\Capsule\Manager as Capsule;

use PHPUnit_Framework_TestCase;
use TryCatch\Task_Two\Controllers\AddressController;
use Silex\Application;

class AddressControllerTest extends PHPUnit_Framework_TestCase
{
    protected $addressController;

    protected function setUp()
    {
        $this->addressController = new AddressController(new Application());

        $capsule = new Capsule;

        $capsule->addConnection([
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'trycatch_test',
            'username'  => 'root',
            'password'  => 'admin',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);

        $capsule->setAsGlobal();
        $capsule->bootEloquent();

        // TODO USE sqlite for test
        // TODO call migrate for have a database with the example.csv data

    }

    public function testGetList()
    {
        // TODO change to assert the view
        $this->assertNotFalse($this->addressController->getList());
    }

    public function testShow()
    {
        // TODO change to assert the view
        $this->assertNotFalse($this->addressController->getShow(1));
    }

    public function testGetCreate()
    {
        // TODO change to assert the view
        $this->assertNotFalse($this->addressController->getCreate());
    }

    public function testPostCreate()
    {
        // TODO
        /**
         * 1. Launche the controller function
         * 2. query database, if row mastching all params exists, it works
         */
        $this->assertEquals(1, 1);
    }

    public function testGetEdit()
    {
        // TODO change to assert the view
        $this->assertNotFalse($this->addressController->getEdit(1));
    }

    public function testPostEdit()
    {
        // TODO
        /**
         * 1. Launch the controller function
         * 2. query database, if the data in row has changed, it works
         */
        $this->assertEquals(1, 1);
    }

    public function testDelete()
    {
        // TODO
        /**
         * 1. launch the controller function
         * 2. query database, if the row is empty, it works
         */
        $this->assertEquals(1, 1);
    }
} 
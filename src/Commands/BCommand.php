<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 24/09/14
 * Time: 23:21
 */

namespace TryCatch\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Make a function that will calculate the power of a number x to index y, but you cannot use
 * multiplication! to make it a bit simpler please take into account only natural numbers.
 *
 * Class BCommand
 * @package TryCatch\Commands
 */
class BCommand extends Command
{
    /**
     * @@inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('b:run')
            ->setDescription('Run Exercise B')
            ->setHelp(<<<EOT
Make a function that will calculate the power of a number x to index y, but you cannot use
multiplication! to make it a bit simpler please take into account only natural numbers.
EOT
            )
            ->addArgument(
                'x',
                InputArgument::OPTIONAL,
                'Number to power'
            )
            ->addArgument(
                'y',
                InputArgument::OPTIONAL,
                'Index'
            )
            ->addOption(
                'extra',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will run not using pow php function'
            )
            ->addOption(
                'extra2',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will run not using pow php function and recursive method'
            )
        ;
    }

    /**
     * @@inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('x') || !$input->getArgument('y')) {
            $output->writeln('<error>Arguments "x" and "y" are required<error>');
            return;
        }

        $base = $input->getArgument('x');
        $index = $input->getArgument('y');

        if ($input->getOption('extra')) {
            $result = $this->manuallyPow($base, $index);
        }
        elseif ($input->getOption('extra2')) {
            $result = $this->recursivePow($base, $index, $base);
        }
        else {
            $result = $this->pow($base, $index);
        }

        $output->writeln('<info>' . $result . '<info>');
    }

    /**
     * Calculate the power of a number x to index y by php function
     *
     * @param $base
     * @param $index
     * @return number
     */
    protected function pow($base, $index)
    {
        return pow($base, $index);
    }

    /**
     * Calculate the power of a number x to index y manually (php function unused)
     *
     * @param $base
     * @param $index
     */
    protected function manuallyPow($base, $index)
    {
        $summatory = $base;

        // We start with value 1 because in the first multiplication we must spend two base values
        // the first is spend on initialize the summatory
        for ($i = 1 ; $i < $index ; $i++) {

            $iterator = $base;
            // On each iteration of the "for" we must declare the new Base for make the summatory
            $newBase  = $summatory;

            // Same here, to simulate the first multiplication we must simulate that we have 1 less index
            while ($iterator-1 > 0) {
                $summatory += $newBase;

                $iterator--;
            }

        }

        return $summatory;
    }

    /**
     * Calculate the power of a number x to index y manually and recursive (php function unused)
     *
     * @param $base
     * @param $index
     * @param $summatory new Base value, same as $base at start
     * @return mixed
     */
    protected function recursivePow($base, $index, $summatory)
    {
        if ($index > 1) {
            $summatory = $this->getIndexSummatory($base, $summatory);

            return $this->recursivePow($base, $index-1, $summatory);
        }

        return $summatory;
    }

    protected function getIndexSummatory($iterator, $newBase)
    {
        $iterator--;

        if ($iterator > 0) {
            return $this->getIndexSummatory($iterator-1, $newBase + $newBase);
        }

        return $newBase;
    }
}
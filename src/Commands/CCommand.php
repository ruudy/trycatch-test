<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 24/09/14
 * Time: 20:27
 */

namespace TryCatch\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Calculate and print 10 numbers for fibonacci series. Use recursion.
 *
 * Extra: Create second algorithm for fibonacci but without recursion.
 *
 * Class CCommand
 * @package TryCatch\Commands
 */
class CCommand extends Command
{
    protected $output;

    /**
     * @@inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('c:run')
            ->setDescription('Run Exercise C')
            ->setHelp(<<<EOT
Calculate and print 10 numbers for fibonacci series. Use recursion.

Extra: Create second algorithm for fibonacci but without recursion.
EOT
            )
            ->addOption(
                'extra',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will run the second algorithm'
            )
        ;
    }

    /**
     * @@inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        $input->getOption('extra')
            ? $this->fibonacci(10)
            : $this->recursiveFibonacci(10);
    }

    /**
     * Show the fibonacci sequence by recursion
     *
     * @param $limit Numbers showed
     * @param array $numbers
     */
    protected function recursiveFibonacci($limit, $numbers = [1, 0])
    {
        if ($limit > 0) {
            $next = array_sum($numbers);

            array_shift($numbers);
            array_push($numbers, $next);

            $this->output->writeln('<info>' . $next . '<info>');

            $this->recursiveFibonacci($limit-1, $numbers);
        }
    }

    /**
     * Show the fibonnaci sequence by iteration
     *
     * @param $limit Numbers showed
     */
    protected function fibonacci($limit)
    {
        $numbers = [1, 0];

        for ( $i = 0; $i < $limit ; $i++) {
            $next = array_sum($numbers);

            array_shift($numbers);
            array_push($numbers, $next);

            $this->output->writeln('<info>' . $next . '<info>');
        }
    }
} 
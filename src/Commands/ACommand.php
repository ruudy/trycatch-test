<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 24/09/14
 * Time: 19:28
 */

namespace TryCatch\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * The sum of all natural numbers below 10 that are multiples of 3 or 5 are 23 (3 + 5 + 6 + 9)
 * Write a php script that will find the sum of all the multiples of 3 or 5 below 1000. The script
 * should run from command line and put the result on screen. We will judge this task based on
 * simplicity, efficiency and cleverness of the code.
 *
 * Class ACommand
 * @package TryCatch\Commands
 */
class ACommand extends Command
{
    /**
     * @@inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('a:run')
            ->setDescription('Run Exercise A')
            ->setHelp(<<<EOT
The sum of all natural numbers below 10 that are multiples of 3 or 5 are 23 (3 + 5 + 6 + 9)
 * Write a php script that will find the sum of all the multiples of 3 or 5 below 1000. The script
 * should run from command line and put the result on screen. We will judge this task based on
 * simplicity, efficiency and cleverness of the code.

 Extra: Create a second algorithm to find the sum of all the multiples of 3 and 4 below 1000.
EOT
            )
            ->addOption(
                'extra',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will run the second algorithm'
            )
        ;
    }

    /**
     * @@inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $result = ($input->getOption('extra'))
            ? $this->calculate(3, 4, 1000)
            : $this->calculate(3, 5, 1000);

        $output->writeln('<info>' . $result . '<info>');
    }

    /**
     * Calculate the summatory of all natural numbers below one number that are multiples of another two
     *
     * @param $firstDivisor First Divisor
     * @param $secondDivisor Second Divisor
     * @param $limit Number below summatory will be calculated
     * @return int
     */
    protected function calculate($firstDivisor, $secondDivisor, $limit) {
        $summatory = 0;

        for ($i = 1 ; $i < $limit; $i++) {
            if ( $i % $firstDivisor == 0 || $i % $secondDivisor == 0) {
                $summatory += $i;
            }
        }

        return $summatory;
    }
}
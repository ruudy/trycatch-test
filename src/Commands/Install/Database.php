<?php

namespace TryCatch\Commands\Install;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Illuminate\Database\Capsule\Manager as Capsule;

class Database extends Command
{
    protected function configure()
    {
        $this
            ->setName('trycatch:database:create')
            ->setDescription('Create tables')
            ->addOption(
                'delete',
                null,
                InputOption::VALUE_NONE,
                'If set, drop tables before creation');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('delete')) {
            try {
                $this->deleteTables();
            } catch (\Exception $e) {
                $output->writeln("Can't delete tables: " . $e->getMessage());
            }
        }

        try {
            $this->createTables();
            $output->writeln("Created!");

        } catch(\Exception $e) {
            $output->writeln("Error: " . $e->getMessage());
        }
    }

    /**
     * Delete the schema tables
     */
    protected function deleteTables()
    {
        Capsule::schema()->dropIfExists('addresses');
    }

    /**
     * Create the schema tables
     */
    protected function createTables()
    {
        Capsule::schema()->create('addresses', function($table)
        {
            $table->increments('id');
            $table->string('name', 64);
            $table->integer('phone')->unsigned();
            $table->string('street', 150);
            $table->timestamps();
        });
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: ruudy
 * Date: 29/09/14
 * Time: 23:18
 */

namespace TryCatch\Commands\Install;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use TryCatch\Task_Two\Models\Address;

class Seed extends Command
{
    protected function configure()
    {
        $this
            ->setName('trycatch:database:seed')
            ->setDescription('Seed tables with sample data')
            ->addOption(
                'truncate',
                null,
                InputOption::VALUE_NONE,
                'If set, drop tables before creation');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('truncate')) {
            Address::truncate();
        }

        $this->seedAddresses();

        $output->writeln("seeded!");
    }

    /**
     * Insert some rows in address table for testing purposes
     */
    protected function seedAddresses()
    {
        // TODO sacar del example.csv

        $address = new Address(
            [
                'name' => 'Michal',
                'phone' => '506088156',
                'street' => 'Michalowskiego 41',
            ]
        );
        $address->save();

        $address = new Address(
            [
                'name' => 'Marcin',
                'phone' => '502145785',
                'street' => 'Opata Rybickiego 1',
            ]
        );
        $address->save();

        $address = new Address(
            [
                'name' => 'Piotr',
                'phone' => '504212369',
                'street' => 'Horacego 23',
            ]
        );
        $address->save();

        $address = new Address(
            [
                'name' => 'Albert',
                'phone' => '605458547',
                'street' => 'Jan Pawła 67',
            ]
        );
        $address->save();
    }
} 
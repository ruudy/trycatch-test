<?php
// exercise.php

require_once 'vendor/autoload.php';
require_once 'config/database.php';

use Symfony\Component\Console\Application;
use TryCatch\Commands\ACommand;
use TryCatch\Commands\BCommand;
use TryCatch\Commands\CCommand;

use TryCatch\Commands\Install\Database;
use TryCatch\Commands\Install\Seed;

$application = new Application();

$application->add(new ACommand);
$application->add(new CCommand);
$application->add(new BCommand);

$application->add(new Database);
$application->add(new Seed);

$application->run();